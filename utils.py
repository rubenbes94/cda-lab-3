import pandas as pd
import numpy as np
from nltk import ngrams
import sys


def reservoir(stream, k):
    sample = [None]*k
    for i in range(len(stream)):
        if i < k:
            sample[i] = stream.loc[i]
        else:
            j = np.random.randint(0, i)
            if j < k:
                sample[j] = stream.loc[i]
    return pd.DataFrame(sample)


def apply_grams(gram_num, train, test):
    # For each column, we create the ngrams, aggregate and normalize
    train_grams = {}
    train_grams = {}
    # Create the ngrams
    grams = list(ngrams(train, gram_num))
    s = len(grams)
    for i, gram in enumerate(grams):
        if np.random.rand()<0.01:
            sys.stdout.write("\rtraining progress: {:8.2f}%".format(i*100/s))
        # If it is new, add it
        if gram not in train_grams:
            train_grams[gram] = 0
        # Increment and normalize
        train_grams[gram] += 1 / s
    
    # Test using train_2
    print("")
    marked = np.zeros(test.shape)
    grams = list(ngrams(test, gram_num))
    for i, gram in enumerate(grams):
        if np.random.rand()<0.01:
            sys.stdout.write("\rtesting progress: {:8.2f}%".format(i*100/len(grams)))
        # If the current ngram does not exist in the ngrams of train_1, we have an anomaly
        if gram not in train_grams:
            # We mark that we found an anomaly in this sensor at this point in time
            marked[i] += 1

    return (marked >= 1)
