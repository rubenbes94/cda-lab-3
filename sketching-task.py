# Build code for computing a COUNT-MIN sketch, play with different heights and widths for the Count-Min sketch matrix.
# Compare it to the RESERVOIR sampling strategy. Is it more spaceefficient/accurate? What about run-time? Use the theory
# to explain any differences you observe.
from pathlib import Path

import pandas as pd
import numpy as np
import math
import mmh3
# MMH3 is a wrapper library, so it requires the install of visual c++ 14.0, you can install this using
# the build tools: https://visualstudio.microsoft.com/visual-cpp-build-tools/


def load_cm(epsilons, deltas):
    cm = {}
    for epsilon in epsilons:
        width = math.ceil(math.e / epsilon)
        for delta in deltas:
            depth = math.ceil(math.log(1 / delta))
            # If the file does not exist yet, we specifically create it. This can take a few minutes
            if not Path("data/minsketch/w%dd%d.npy" % (width, depth)).is_file():
                cm["%d,%d" % (width, depth)] = count_min_sketch(df, width, depth)
                np.save("data/minsketch/w%dd%d" % (width, depth), cm["%d,%d" % (width, depth)])
                print("Created matrix for w=%d, d=%d" % (width, depth))
            else:
                cm["%d,%d" % (width, depth)] = np.load("data/minsketch/w%dd%d.npy" % (width, depth))
    return cm


def count_min_sketch(event_df, w, d):
    # Initialize matrix
    cm = np.zeros((d, w))

    # For each item/event
    for i in range(len(event_df)):
        event = df["Dst IP Addr"][i]  # We define an event by its 'Dst IP Addr', as we compare it to reservoir
        # Apply each hash function
        for j in range(d):
            cm[j, mmh3.hash(event, j) % w] += 1
        if i % 10000 == 0:
            print("Now at i=%d" % i)
    return cm


def point_query(cm, event):
    (d, w) = cm.shape
    # We simply perform each hash and select the lowest value
    return np.min([cm[j, mmh3.hash(event, j) % w] for j in range(d)])


def test_accuracy(events, print_formatted_accuracy=False):
    actual_counts = df["Dst IP Addr"].value_counts()
    diff = {}
    percentages = {}

    for ip_address in events:
        # Check how often this ip address actually occurs
        actual_count = actual_counts[ip_address]
        for epsilon in epsilons:
            width = math.ceil(math.e / epsilon)
            for delta in deltas:
                depth = math.ceil(math.log(1 / delta))
                if "%d,%d" % (width, depth) not in diff:
                    diff["%d,%d" % (width, depth)] = 0
                if "%d,%d" % (width, depth) not in percentages:
                    percentages["%d,%d" % (width, depth)] = 0
                # Calculate the count according to count_min_sketch
                count_min = point_query(cm["%d,%d" % (width, depth)], ip_address)
                # Calculate the difference between count_min and the actual count, as well as the percentages off
                diff["%d,%d" % (width, depth)] += (count_min - actual_count) / len(events)
                percentages["%d,%d" % (width, depth)] += 100 * (count_min / actual_count) / len(events)

    # If print_formatted_accuracy is True then we print a formatted string to copy into LaTeX
    if print_formatted_accuracy:
        string = ""
        i = 1
        for key in diff:
            string += "%.1f \\textit{%.1f}\\%% & " % (diff[key], percentages[key])
            if i % len(deltas) == 0:
                string = string[:-3] + "\n"
            i += 1
        string = string[:-3]
        print(string)
    return diff, percentages


# Load the data
df = pd.read_csv("data/dataset52_processed.csv")
# Set column names
df.columns = ["Date Flow Start", "Duration", "Protocol", "Src IP Addr", "Dst IP Addr", "Flags", "Tos",
              "Packets", "Bytes", "Flows", "Label", "Src Port", "Dst Port"]

# We use the epsilon and delta instead of width and depth because these hold more mathematical significance
epsilons = [1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001]  # maximum value off of actual value when performing point query
deltas = [1E-1, 1E-2, 1E-3, 1E-4, 1E-5]  # chance to be at most epsilon off of actual value
cm = load_cm(epsilons, deltas)

# Test a few events
ip_addresses_frequent = ["147.32.96.69", "147.32.80.9", "147.32.84.229", "147.32.86.116", "147.32.84.59"]
ip_addresses_infrequent = ["72.252.216.149", "80.79.27.52", "91.77.94.94", "147.213.65.69", "98.155.209.16"]

diff_frequent, percentages_frequent = test_accuracy(ip_addresses_frequent)
diff_infrequent, percentages_infrequent = test_accuracy(ip_addresses_infrequent)
