# Pick any of the CTU-13 datasets (Malware capture 42 to 54). Download the unidirectional netflows. DO NOT DOWNLOAD THE
# VIRUS THAT WAS USED TO GENERATE THE DATA UNLESS USING A VM OR OTHER SANDBOX. The flows are collected from a host in
# the network. Its IP address should be obvious from the data sample. We are interested in the other addresses the
# host connects with.
# Estimate the distribution over the other IP_addresses, what are the 10 most frequent values? Write code for RESERVOIR
# sampling, use it to estimate the distribution in one pass (no need to actually stream the data, you may store it in
# memory, or run every file separately, but do store and load the intermediate results). Use a range of reservoir sizes.
# What are the 10 most frequent IP-addresses and their frequencies when sampled? Use the theory to explain any
# approximation errors you observe.
import pandas as pd
import io
import numpy as np
from utils import reservoir
import re
from pathlib import Path

# Preprocess only when file does not exist
if not Path("data/dataset52_processed.csv").is_file():
    print("Make sure to put capture20110818-2.pcap.netflow.labeled in data directory! http\
s://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-52/capture20110818\
-2.pcap.netflow.labeled")
    with io.open("data/capture20110818-2.pcap.netflow.labeled", "r") as unprocessed_data, \
            io.open("data/dataset52_processed.csv", "w") as processed_data:
        print("Preprocessing data (this might take a few mins)...")
        first_line = True
        for line in unprocessed_data:
            if first_line:
                # Skip header line
                first_line = False
            else:
                # Replace the "->" and the (multiple) surrounding tabs with a ","
                line = re.sub(r"\t+->\t+", ",", line)
                # Replace the other (multiple) tabs with a ","
                line = re.sub(r"\t+", ",", line)

                # Remove newline symbol
                line = line[:-1]
                # Extract port number if applicable. There are some MAC-addresses in the file so we check if there is
                # exactly 1 ":" in the src and dst addresses. If there are zero or more than 1 we set the port number
                # to -1 and leave the entire address as the IP-address
                split = line.split(",")
                src = split[3].split(":")
                dst = split[4].split(":")
                src_port = "-1"
                dst_port = "-1"
                if len(src) == 2:
                    src_port = src[-1]
                    split[3] = ":".join(src[:-1])
                if len(dst) == 2:
                    dst_port = dst[-1]
                    split[4] = ":".join(dst[:-1])

                split.append(src_port)
                split.append(dst_port)
                line = ",".join(split) + "\n"
                # Write line to file
                processed_data.write(line)

df = pd.read_csv("data/dataset52_processed.csv")
# Set column names
df.columns = ["Date Flow Start", "Duration", "Protocol", "Src IP Addr", "Dst IP Addr", "Flags", "Tos",
              "Packets", "Bytes", "Flows", "Label", "Src Port", "Dst Port"]

sample = reservoir(df, 100)
# Count how often each outgoing IP occurs
print(df["Dst IP Addr"].value_counts().head(10))

print("Total entries {}".format(len(df)))
print(df["Dst IP Addr"].value_counts().head(10).values/len(df) * 100)
