# Choose a probabilistic sequential model (Markov chain, n-grams, state machines, HMMs, …). Code for HMMs is available
# in many packages, or you can use our own Python code from Brightspace. For state machines you may use our code for
# state machine learning from https://bitbucket.org/chrshmmmr/dfasat. Use a sliding window to obtain sequence data.
# Learn a probabilistic sequential model from the data of one infected host and match its profile (as discussed in
# class) with all other hosts from the same scenario. Evaluate how many new infections your method finds and false
# positives it raises (as in paper 4). Can you determine what behaviour your profile detects?


#Note:  Use the discretisation from the previous exercise in order to learn a sequential model like HMM, DFA
import pandas as pd
import io
import numpy as np
from utils import reservoir
import re
from pathlib import Path
from utils import apply_grams
from nltk import ngrams
from sklearn.metrics import confusion_matrix, f1_score

if not Path("data/dataset52_processed.csv").is_file():
    print("Please run sampling-task.py first before running this one to create necessary files")
    exit(1)

df = pd.read_csv("data/dataset52_processed.csv")
# Set column names
df.columns = ["Date Flow Start", "Duration", "Protocol", "Src IP Addr", "Dst IP Addr", "Flags", "Tos",
              "Packets", "Bytes", "Flows", "Label", "Src Port", "Dst Port"]


df["Bytes-binned"] = np.floor(np.log2(df["Bytes"]+1))
df["Dst Port-binned"] = np.floor(df["Dst Port"]/512)
df["Src Port-binned"] = np.floor(df["Src Port"]/512)
df["ngram1"] = df["Flags"].map(str)+"-"+ df["Bytes-binned"].map(str)+"-"+df["Dst Port-binned"].map(str)
df["ngram2"] = df["Flags"].map(str)+"-"+ df["Bytes-binned"].map(str)+"-"+df["Dst Port-binned"].map(str)+"-"+df["Src Port-binned"].map(str)
df["ngram3"] = df["Flags"].map(str)+"-"+ df["Bytes-binned"].map(str)

background = pd.concat([df[df["Label"] == "Background"], df[df["Label"] == "LEGITIMATE"]])
botnet = df[df["Label"] == "Botnet"]

for k in range(1,4):
    for i in range(2,5):
        print(f"\n ### {i}-gram{k}\n")
        prediction = apply_grams(i, background[f"ngram{k}"], df[f"ngram{k}"])
        df["truth"] = df["Label"] == "Botnet"
        print(confusion_matrix(df["truth"].values, prediction).ravel())
        print(f1_score(df["truth"].values, prediction))