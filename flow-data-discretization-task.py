# We aim to learn a sequential model from NetFlow data from an infected host (unidirectional netflows). Consider
# scenario 10 from the CTU-13 data sets (see paper 4 from below resources). Remove all background flows from the data.
# You are to discretize the NetFlows. Investigate the data from one of the infected hosts. Select and visualize two
# features that you believe are most relevant for modeling the behavior of the infected host. Discretize these features
# using use any of the methods discussed in class (combine the two values into a single discrete value). Do you observe
# any behavior in the two features that could be useful for detecting the infection? Explain. Apply the discretization
# to data from all hosts in the selected scenario.

import os
from pathlib import Path
import numpy as np
import pandas as pd
import seaborn.apionly as sns
import matplotlib.pyplot as plt
import collections

if not Path("data/2013-08-20_capture-win7.weblogng.labeled").is_file():
    print("To run this script, please download and put in data/ directory: https://mcfp.felk.cvut.cz/publicDatasets/CTU-Malware-Capture-Botnet-10/2013-08-20_capture-win7.weblogng.labeled")
    exit(1)


df = pd.read_csv("data/2013-08-20_capture-win7.weblogng.labeled", sep="|")
print(df.head())

# print(df["s-ip"].value_counts().head(100))
# print(df["sc-bytes"].value_counts().head(100))
df["sc-bytes-binned"] = np.floor(np.log2(df["sc-bytes"]+1))
df["s-ip-binned"] = [".".join(x.split(".")[0:3])+".***" for x in df["s-ip"].values]
df["bin"] = [str(df["sc-bytes-binned"].loc[i])+df["s-ip-binned"].loc[i] for i in range(len(df))]
print(len(df["s-ip-binned"].value_counts()))
freqs_lookup = df["bin"].value_counts().to_dict()
freqs = [0]*len(df["bin"])
for i in range(len(df["bin"].values)):
    freqs[i] = np.log2(freqs_lookup[df["bin"].values[i]])
df["bin-freqs"] = freqs
piv = pd.pivot_table(df, values="bin-freqs",columns=["s-ip-binned"], index=["sc-bytes-binned"], fill_value=0)
#plot pivot table as heatmap using seaborn

ax = sns.heatmap(piv, square=True)
ax.set_yticklabels(["{} - {}".format(int(2**k-1),int(2**(k+1)-1)) for k,v in collections.OrderedDict(sorted(df["sc-bytes-binned"].value_counts().to_dict().items())).items()], rotation=90)
ax.set_ylabel("packet size in bytes")
ax.set_xlabel("IP address")
ax.set_title("Frequency of packetsize/IP-address combination in logarithmic scale")
plt.setp( ax.xaxis.get_majorticklabels(), rotation=90 )
plt.setp( ax.yaxis.get_majorticklabels(), rotation=0 )
plt.tight_layout()
plt.show()