# Study paper 3 and construct a classifier for detecting anomalous behavior in individual NetFlows (every flow is a row,
# ignoring sequences). Do not forget to study and deal with properties of your data such as class imbalance. Evaluate
# your method in two ways: on the packet level (as in paper 3), and on the host level (as in paper 4). Do you prefer
# using a sequential model or a classifier for detecting botnets? Explain why.
